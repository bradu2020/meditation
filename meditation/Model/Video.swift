//
//  Video.swift
//  meditation
//
//  Created by XeVoNx on 12/07/2020.
//  Copyright © 2020 XeVoNx. All rights reserved.
//

import UIKit

struct Video {
    var vdeoId = ""
    var title = ""
    var description = ""
    var thumbnail = ""
    var published = ""
}
