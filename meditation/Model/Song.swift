//
//  Song.swift
//  meditation
//
//  Created by XeVoNx on 23/07/2020.
//  Copyright © 2020 XeVoNx. All rights reserved.
//

import UIKit

struct Song {
    let imageName: String
    let trackName: String
    let displayName: String
}
