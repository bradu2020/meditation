//
//  Category.swift
//  meditation
//
//  Created by XeVoNx on 26/06/2020.
//  Copyright © 2020 XeVoNx. All rights reserved.
//

import UIKit

struct Category {
    var title: String
    var url: String?
    var mediaType: mediaType?
    var bgColor: UIColor?
    
    enum mediaType {
        case yt
        case mp3
    }
    
    init(title: String, url:String? = nil, mediaType: mediaType? = nil, bgColor: UIColor? = nil) {
        self.title = title
        self.url = url
        self.mediaType = mediaType
        self.bgColor = bgColor
        }
        
    
}


