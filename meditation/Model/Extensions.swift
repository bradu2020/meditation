//
//  Extensions.swift
//  meditation
//
//  Created by XeVoNx on 29/06/2020.
//  Copyright © 2020 XeVoNx. All rights reserved.
//

import UIKit

extension UIViewController {

    func addBackButton() {
        
        let btnLeftMenu: UIButton = UIButton()
        //let image = UIImage(named: "backButtonImage");
        //btnLeftMenu.setImage(image, for: .normal)
        btnLeftMenu.setTitle("Back", for: .normal);
        btnLeftMenu.sizeToFit()
        btnLeftMenu.setTitleColor(#colorLiteral(red: 0.9719226956, green: 0.5081535578, blue: 0.02913753875, alpha: 1), for: .normal)
        btnLeftMenu.titleLabel?.font = UIFont(name: "System", size: 50)
        
        btnLeftMenu.addTarget(self, action: #selector (backButtonClick(sender:)), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }

    @objc func backButtonClick(sender : UIButton) {
        presentingViewController?.dismiss(animated: true)
    }
}



