//
//  ViewController.swift
//  meditation
//
//  Created by XeVoNx on 11/06/2020.
//  Copyright © 2020 XeVoNx. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let categories: [Category] = [Category(title:"NOU: Meditatia zilei",mediaType: .yt, bgColor: #colorLiteral(red: 0.9719226956, green: 0.5081535578, blue: 0.02913753875, alpha: 1)),Category(title:"Cum sa meditezi-instructiuni",mediaType: .mp3, bgColor: #colorLiteral(red: 0.2345532477, green: 0.5335883498, blue: 0.8767972589, alpha: 1) ),Category(title:"Incepe sa meditezi!", mediaType: .mp3, bgColor: #colorLiteral(red: 0.9677298665, green: 0.3044395447, blue: 0.3086183071, alpha: 1)),Category(title:"Grupul de mediatie online",url:"http://www.stefan-pusca.ro/grup-de-meditatie-online/", bgColor: #colorLiteral(red: 0.917540133, green: 0.1847516298, blue: 0.9338641763, alpha: 1)),Category(title:"Respiratie constienta", mediaType: .mp3, bgColor: #colorLiteral(red: 0.4778672457, green: 0.6217842698, blue: 0.7970417142, alpha: 1)),Category(title:"Trezirea atentiei de sine", mediaType: .mp3, bgColor:#colorLiteral(red: 0.8732511401, green: 0.4842943549, blue: 0.2325119972, alpha: 1)),Category(title:"Conferinta gratuita lunara",url: "http://www.stefan-pusca.ro/conferinta-lunara-gratuita",bgColor: #colorLiteral(red: 0.2316583395, green: 0.750444591, blue: 0.9344479442, alpha: 1)),Category(title:"Meditatii complexe",mediaType: .mp3,bgColor:#colorLiteral(red: 0.6162521243, green: 0.6828018427, blue: 0.1379570365, alpha: 1)),Category(title:"Cartea Terapia prin meditatie", url: "https://www.edituraparalela45.ro/produs/terapia-prin-meditatie-sanatate-si-echilibru-emotional-prin-procedee-simple-de-relaxare-respiratie-si-gandire-pozitiva/",bgColor:#colorLiteral(red: 0.5623095632, green: 0.5591369867, blue: 0.914989233, alpha: 1)),Category(title:"Ghid video de meditatie", mediaType:  .yt, bgColor:#colorLiteral(red: 0.711522758, green: 0.2408490479, blue: 0.274456352, alpha: 1)),Category(title:"Facebook",url:"https://web.facebook.com/Cursuri-stefan-puscaro-1391509794453288",bgColor:#colorLiteral(red: 0.2916699946, green: 0.4144387841, blue: 0.6460009217, alpha: 1)),Category(title:"www.stefanpusca.ro",url:"http://www.stefanpusca.ro",bgColor: #colorLiteral(red: 0.9788758159, green: 0.6995337009, blue: 0.06099034101, alpha: 1)),Category(title:"Contact", bgColor: #colorLiteral(red: 0.9406040311, green: 0.5302391648, blue: 0.3542448282, alpha: 1)),Category(title:"Despre aplicatie",bgColor:#colorLiteral(red: 0.4799407125, green: 0.6210083365, blue: 0.441072762, alpha: 1) ),Category(title: "Share",bgColor: #colorLiteral(red: 0.5006632805, green: 0.4773791432, blue: 0.7234604955, alpha: 1))]
    
    var songs = [Song]()
    var vidIdList = [String]()

    fileprivate func configTableView() {
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configTableView()
        
    }

    @IBOutlet weak var tableView: UITableView! 
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }

     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoryTableViewCell
             
        cell.titleLabel.text = categories[indexPath.row].title
        cell.backgroundColor = categories[indexPath.row].bgColor

            return cell
        }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {

        if let urlStr = categories[indexPath.row].url, let url = URL(string: urlStr) {
            
            guard UIApplication.shared.canOpenURL(url) else {
              let alert = UIAlertController(title: "Warning", message: "Problem with URL.",   preferredStyle: .alert)
              alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
              self.present(alert, animated: true)
              return
            }
            
            UIApplication.shared.open(url, options: [:])
        } else if categories[indexPath.row].title == "Share" {
            if let urlStr = NSURL(string: "https://apps.apple.com/ro/app/train-valley-2/id1506277389?mt=12") {
                let objectsToShare = [urlStr]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)

                if UIDevice.current.userInterfaceIdiom == .pad {
                    if let popup = activityVC.popoverPresentationController {
                        popup.sourceView = self.view
                        popup.sourceRect = CGRect(x: self.view.frame.size.width / 2, y: self.view.frame.size.height / 4, width: 0, height: 0)
                    }
                }

                self.present(activityVC, animated: true, completion: nil)
            }
        } else if categories[indexPath.row].title == "Contact" {
            performSegue(withIdentifier: "goToContact", sender: indexPath.row)
        } else if categories[indexPath.row].title == "Despre aplicatie" {
            performSegue(withIdentifier: "goToDespreAplicatie", sender: indexPath.row)
        } else {
          
        switch categories[indexPath.row].mediaType {
          case .yt:
            if categories[indexPath.row].title == "NOU: Meditatia zilei" {
                print("List 1 YT")
                vidIdList = ["Pvava_kj5TQ","7tBfX8oQUkw","S6G0DtCzjvM","sfcklbmU4r4","Yvszbo55694","0T8v_4PSnAY","9soRFeYIJRI","9tt85CpoUb8","E7IM1uiDzTo"]
            } else if categories[indexPath.row].title == "Ghid video de meditatie" {
                print("List 2 YT")
                vidIdList = ["GteIM57f2u0","U05YqSGLvVE"]
            }
              performSegue(withIdentifier: "goToYoutubeList", sender: indexPath.row)
          case .mp3:
            
            if categories[indexPath.row].title == "Cum sa meditezi-instructiuni" {
                songs = [
                    Song(imageName: "med24", trackName: "w24", displayName: "Ce este meditatia"),
                    Song(imageName: "med26", trackName: "w26", displayName: "15 secrete ale meditatiei pentru incepatori"),
                    Song(imageName: "med5", trackName: "w5", displayName: "Reguli privind spatiul de meditatie"),
                    Song(imageName: "med27", trackName: "w27", displayName: "10 subtilitati ale pozitilor de meditatie"),
                    Song(imageName: "med20", trackName: "w20", displayName: "Cele 7 secrete ale pozitiei de meditatie"),
                    Song(imageName: "med23", trackName: "w23", displayName: "Ce este minirelaxarea"),
                    Song(imageName: "med16", trackName: "w16", displayName: "Cum se relaxeaza un muschi"),
                    Song(imageName: "med18", trackName: "w18", displayName: "Cele 10 semne ale unei respiratii deficiente"),
                    Song(imageName: "med22", trackName: "w22", displayName: "Cele 5 reguli pentru a respira corect"),
                    Song(imageName: "med4", trackName: "w4", displayName: "Respiratia abdominala-instructiuni"),
                    Song(imageName: "med19", trackName: "w19", displayName: "Cele 9 secrete ale respiratiei terapeutice"),
                    Song(imageName: "med2", trackName: "w2",displayName:"Respiratia semialternata-instructiuni"),
                    Song(imageName: "med17", trackName: "w17", displayName: "Cum se face respiratia completa"),
                    Song(imageName: "med10", trackName: "w10", displayName: "Numararea respiratiilor-instructiuni"),
                    Song(imageName: "med12", trackName: "w12", displayName: "Meditatia so-ham-instructiuni")
                ]
            } else if categories[indexPath.row].title == "Incepe sa meditezi!" {
                songs = [
                    Song(imageName: "med11", trackName: "w11", displayName: "Minirelaxarea-practica"),
                    Song(imageName: "med14", trackName: "w14", displayName: "Meditatia cu palmele in pozitia rugaciunii"),
                    Song(imageName: "med8", trackName: "w8", displayName: "Practica respiratiei abdominale"),
                    Song(imageName: "med15", trackName: "w15", displayName: "Exercitiu de respiratie constienta"),
                    Song(imageName: "med25", trackName: "w25", displayName: "Ascultarea respiratiei"),
                    Song(imageName: "med6", trackName: "w6", displayName: "Practica respiratiei semialternate"),
                    Song(imageName: "med3", trackName: "w3", displayName: "Respiratia constienta pe 3 nivele"),
                    Song(imageName: "med7", trackName: "w7", displayName: "Practica respiratiei complete"),
                    Song(imageName: "med10", trackName: "w10", displayName: "Numararea respiratiilor-instructiuni"),
                    Song(imageName: "med13", trackName: "w13", displayName: "Meditatia so-ham practica")
                ]
            } else if categories[indexPath.row].title == "Respiratie constienta" {
                songs = [
                    Song(imageName: "med8", trackName: "w8", displayName: "Practica respiratiei abdominale"),
                    Song(imageName: "med15", trackName: "w15", displayName: "Exercitiu de respiratie constienta"),
                    Song(imageName: "med25", trackName: "w25", displayName: "Ascultarea respiratiei"),
                    Song(imageName: "med6", trackName: "w6", displayName: "Practica respiratiei semialternate"),
                    Song(imageName: "med3", trackName: "w3", displayName: "Respiratia constienta pe 3 nivele"),
                    Song(imageName: "med7", trackName: "w7", displayName: "Practica respiratiei complete"),
                    Song(imageName: "med9", trackName: "w9", displayName: "Numararea respiratiilor-practica"),
                    Song(imageName: "med13", trackName: "w13", displayName: "Meditatia so-ham practica")
                ]
            } else if categories[indexPath.row].title == "Trezirea atentiei de sine" {
                songs = [
                    Song(imageName: "med25", trackName: "w25", displayName: "Ascultarea respiratiei"),
                    Song(imageName: "med3", trackName: "w3", displayName: "Respiratia constienta pe 3 nivele"),
                    Song(imageName: "med9", trackName: "w9", displayName: "Numararea respiratiilor-practica"),
                    Song(imageName: "med13", trackName: "w13", displayName: "Meditatia so-ham practica")
                ]
            } else if categories[indexPath.row].title == "Meditatii complexe" {
                songs = [
                    Song(imageName: "med1", trackName: "w1", displayName: "O calatorie in ocean")
                ]
            }
              performSegue(withIdentifier: "goToMp3List", sender: indexPath.row)
          case .none:
              return
          }
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
   
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "goToYoutubeList") {
            if let destinationVC = segue.destination as? UINavigationController {
                if let targetController = destinationVC.topViewController as? SelectedCategoryViewController {
                    targetController.list = vidIdList
                    targetController.strType = "Youtube"
                }
            }
        } else if (segue.identifier == "goToMp3List") {
            if let destinationVC = segue.destination as? UINavigationController {
                if let targetController = destinationVC.topViewController as? SelectedCategoryViewController{
                    targetController.strType = "MP3"
                    targetController.songs = songs
                }
            }
        }
        else if (segue.identifier == "goToContact") {
            if let destinationVC = segue.destination as? UINavigationController {
                if let targetController = destinationVC.topViewController as? SelectedCategoryViewController {
                    targetController.contactInfoList = [(title:"Name",subtitle:"Stefan Pusca",icon:UIImage()),(title:"Email",subtitle:"stpusca@yahoo.com",icon:UIImage()),(title:"Website",subtitle:"www.stefanpusca.ro",icon:UIImage())]
                    targetController.strType = "Contact"
                }
            }
        }
    }
    
}
