//
//  PlayerViewController.swift
//  music_player_demo
//
//  Created by XeVoNx on 30/06/2020.
//  Copyright © 2020 XeVoNx. All rights reserved.
//

import UIKit
import AVFoundation

class PlayerViewController: UIViewController {

    public var position: Int = 0 {
        didSet {
            backButton.isHidden = position == 0 ? true : false
            nextButton.isHidden = position == songs.count - 1 ? true : false
        }
    }
    public var songs: [Song] = []
    
    @IBOutlet var holder: UIView!
    
    var player: AVAudioPlayer?
    
    //For tracking current time played
    var timer: Timer! //invalidate when audio file finishes playing or back/next tapped
    
    var changeBgFromPlayerNext: ((Bool) -> Void)?
    var changeBgFromPlayerBack: ((Bool) -> Void)?
    
    //User interface elements, global, dynamic updated
    
    private let albumImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private let songNameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0 //line wrap (multiple lines vs ...)
        return label
    }()
    
    
    private var progressLabel = UILabel() //set in config, updated by timer
    private var slider = UISlider()
    private var audioFileDuration = 0.0
    
    //Player controls
    let nextButton = UIButton()
    let backButton = UIButton()
    let playPauseButton = UIButton()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let blurEffect = UIBlurEffect(style: .regular)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = view.bounds
        blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //view.addSubview(blurView)
        view.insertSubview(blurView, at: 0)
    }
    
    override func viewDidLayoutSubviews() {
        //called once subviews in sb are layed out (ex: holder)
        super.viewDidLayoutSubviews()
        //when this vc loads, holder will be empty
        if holder.subviews.count == 0 {
            configure()
        }
    }
    
    func configure() {
        //set up player
        let song = songs[position]
        
        let urlString = Bundle.main.path(forResource: song.trackName, ofType: "mp3")
        
        view.layer.contents = #imageLiteral(resourceName: song.imageName).cgImage
        
        audioFileDuration = duration(for: urlString!)
        
        do {
            try AVAudioSession.sharedInstance().setMode(.default)
            try AVAudioSession.sharedInstance().setActive(true, options: .notifyOthersOnDeactivation)
            
            guard let urlString = urlString else {
                print("urlString is nil")
                return }
            
            print("urlString: \(urlString)")
            let x = URL(string: urlString)
            print("X is === \(String(describing: x))")
            player = try AVAudioPlayer(contentsOf: URL(string: urlString)!)
            
            guard let player = player else {
                print("Player is nil")
                return }
            
            //player.volume = 0.5
            
            player.play()
            if player.isPlaying {
                timer?.invalidate()
                timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
            }
            
        } catch {
            print("Err getting audio")
        }
        
        //set up user interface elements
        
        //album cover
        albumImageView.frame = CGRect(
            x: 10,
            y: 10,
            width: holder.frame.size.width-20,
            height: holder.frame.size.width-20)
        albumImageView.image = UIImage(named: song.imageName)
        holder.addSubview(albumImageView)
        
        //Labels
        songNameLabel.frame = CGRect(
            x: 10,
            y: albumImageView.frame.size.height + 40,
            width: holder.frame.size.width-20,
            height: 40)
        
        songNameLabel.text = song.displayName
        holder.addSubview(songNameLabel)
        
        //Frames
        let yPosition = holder.frame.size.height-60
        let size: CGFloat = 30
        let playPauseSize: CGFloat = 1.5 * size
        let pBtwBtns: CGFloat = 55
        
        playPauseButton.frame = CGRect(
            x: (holder.frame.size.width - playPauseSize) / 2.0,
            y: yPosition - 10,
            width: playPauseSize,
            height: playPauseSize)
        
        nextButton.frame = CGRect(
            x: playPauseButton.center.x + pBtwBtns, //holder.frame.size.width - size - 20,
            y: yPosition,
            width: size,
            height: size)
        
        backButton.frame = CGRect(
            x: playPauseButton.center.x - pBtwBtns - playPauseSize/2, //20,
            y: yPosition,
            width: size,
            height: size)
            
        //Add actions
        playPauseButton.addTarget(self, action: #selector(didTapPlayPauseButton), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(didTapNextButton), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(didTapBackButton), for: .touchUpInside)

        //Styling
        playPauseButton.setBackgroundImage(UIImage(systemName: "pause.fill"), for: .normal)
        nextButton.setBackgroundImage(UIImage(systemName: "forward.fill"), for: .normal)
        backButton.setBackgroundImage(UIImage(systemName: "backward.fill"), for: .normal)
        
        playPauseButton.tintColor = .black
        backButton.tintColor = .black
        nextButton.tintColor = .black

        
        holder.addSubview(playPauseButton)
        holder.addSubview(nextButton)
        holder.addSubview(backButton)

        
        //Slider
        slider = UISlider(frame: CGRect(
            x: 55,
            y: songNameLabel.frame.origin.y + 20 + 45,
            width: holder.frame.size.width - 110,
            height: 50))
        
        slider.addTarget(self, action: #selector(didSlideSlider(_:)), for: .valueChanged)
        slider.value = 0.0
        slider.minimumTrackTintColor = .purple
        
        holder.addSubview(slider)
        
        
        //Track slider labels
         progressLabel = UILabel(frame: CGRect(
            x: 7,
            y: slider.frame.origin.y + slider.frame.height/4,
            width: 50,
            height: 20))
        
        let audioLengthLabel = UILabel(frame: CGRect(
            x: holder.frame.size.width + 5 - 50, //slider.frame.origin.x + slider.frame.width + 10,
            y: slider.frame.origin.y + slider.frame.height/4,
            width: 50,
            height: 20))
        
        progressLabel.text = "00:00"
        audioLengthLabel.text = getTimeString(from: audioFileDuration)
        progressLabel.font = UIFont.boldSystemFont(ofSize: 14)
        audioLengthLabel.font = UIFont.boldSystemFont(ofSize: 14)
        
        holder.addSubview(progressLabel)
        holder.addSubview(audioLengthLabel)
        
    }
    
    
    @objc func didTapBackButton() {
        //Edge case
        if position > 0 {
            position = position - 1
            player?.stop()
            timer.invalidate()
            for subview in holder.subviews {
                subview.removeFromSuperview()
            }
            configure()
            changeBgFromPlayerBack!(true)
        }
    }
    
    @objc func didTapNextButton() {
        //Edge case, arr starts at 0
        if position < (songs.count - 1) {
            position = position + 1
            player?.stop()
            timer.invalidate()
            for subview in holder.subviews {
                subview.removeFromSuperview()
            }
            configure()
            changeBgFromPlayerNext!(true)
        }
    }
    
    
    
    fileprivate func shrinkImage() {
        //shrink image
        UIView.animate(withDuration: 0.2, animations:  {
            self.albumImageView.frame = CGRect(
                x: 30,
                y: 30,
                width: self.holder.frame.size.width-60,
                height: self.holder.frame.size.width-60)
        })
    }
    
    @objc func didTapPlayPauseButton() {
        if player?.isPlaying == true {
            player?.pause()
            playPauseButton.setBackgroundImage(UIImage(systemName: "play.fill"), for: .normal)

            shrinkImage()
            
            
        } else {
            //player?.play(atTime: TimeInterval(other: slider.value)
            player?.play()
            playPauseButton.setBackgroundImage(UIImage(systemName: "pause.fill"), for: .normal)
            timer.invalidate()
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)

            //increase image size
            UIView.animate(withDuration: 0.2, animations:  {
                self.albumImageView.frame = CGRect(
                x: 10,
                y: 10,
                width: self.holder.frame.size.width-20,
                height: self.holder.frame.size.width-20)
            })

        }
    }
    
    
    var count = 0
    @objc func updateTime() {
        let currentTime = player!.currentTime
        let currentIntTime = Int(currentTime)
        let minutes = currentIntTime/60
        let seconds = currentIntTime - minutes * 60
        
        print("time: \(currentIntTime)")
        progressLabel.text = String(format: "%02d:%02d", minutes,seconds) as String
        slider.value = Float(currentTime / audioFileDuration)
        count += 1
        print(count,currentTime)
        if currentTime >= audioFileDuration - 1 {
            perform(#selector(callbackAudioFinishedPlaying), with: nil, afterDelay: 1)
        }
        if !player!.isPlaying {
            timer.invalidate()
            print("not playing")
        }
    }
    
    @objc func callbackAudioFinishedPlaying() {
        playPauseButton.setBackgroundImage(UIImage(systemName: "play.fill"), for: .normal)
        shrinkImage()
    }
    
    @objc func didSlideSlider(_ slider: UISlider) {
        print("changed to: \(slider.value)")
        player?.currentTime = Double(slider.value) * player!.duration
        playPauseButton.setBackgroundImage(UIImage(systemName: "play.fill"), for: .normal)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //If player isnt nil stop playback befor dismissing this controller
        if let player = player {
            player.stop()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //when threatened to be dismissed player stops
        //if it is not fully dismissed, calls will apear
        guard player != nil else { return }
        player!.play()
        if player!.isPlaying {
            timer.invalidate()
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        timer.invalidate()
    }
    
    func duration(for resource: String) -> Double {
        let asset = AVURLAsset(url: URL(fileURLWithPath: resource))
        return Double(CMTimeGetSeconds(asset.duration))
    }
    
    func getTimeString(from duration: Double) -> String {
        let hour_ = abs(Int(duration/3600))
        let minute_ = abs(Int((duration/60).truncatingRemainder(dividingBy: 60)))
        let second_ = abs(Int(duration.truncatingRemainder(dividingBy: 60)))

//        let hour = hour_ > 9 ? "\(hour_)" : "0\(hour_)"
//        let minute = minute_ > 9 ? "\(minute_)" : "0\(minute_)"
//        let second = second_ > 9 ? "\(second_)" : "0\(second_)"
//        let resultString = hour_ > 0 ? "\(hour):\(minute):\(second)" : "\(minute):\(second)"
//        return resultString
        
        let minute = minute_ > 9 ? "\(minute_ + hour_ * 60)" : "0\(minute_)"
        let second = second_ > 9 ? "\(second_)" : "0\(second_)"
        let resultString = "\(minute):\(second)"
        return resultString
    }
}

