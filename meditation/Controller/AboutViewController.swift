//
//  AboutViewController.swift
//  meditation
//
//  Created by XeVoNx on 28/06/2020.
//  Copyright © 2020 XeVoNx. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    @IBOutlet weak var portraitImageView: UIImageView!
    @IBOutlet weak var aboutScrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addBackButton()
    
        portraitImageView.image = UIImage(named: "elon_musk")
        
    }

}
