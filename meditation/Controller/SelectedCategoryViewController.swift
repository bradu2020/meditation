//
//  CategoryTableViewController.swift
//  meditation
//
//  Created by XeVoNx on 11/06/2020.
//  Copyright © 2020 XeVoNx. All rights reserved.
//

import UIKit
import WebKit //watch yt
import Kingfisher //get+cache images
import MessageUI //send mail

class SelectedCategoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var mediaUrlList = [URL]()
    var thumbnailImageUrlList = [URL]()
    var list = [String]()
    var filteredList: [String] = []
    var strType = ""
    var contactInfoList: [(title:String,subtitle:String,icon:UIImage? )]?
    var bgImg = UIImage(named: "med1")
    var mediaURL: URL!
    var thumbnailImageURL: URL!
    var songs = [Song]()
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView(frame: .zero)
        }
    }
    
    fileprivate func configTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        
        //register cells
        let mp3Nib = UINib(nibName: "Mp3TableViewCell", bundle: nil)
        tableView.register(mp3Nib, forCellReuseIdentifier: "mp3Cell")
        
        let ytNib = UINib(nibName: "YoutubeTableViewCell", bundle: nil)
        tableView.register(ytNib, forCellReuseIdentifier: "youtubeCell")
        
        let contactNib = UINib(nibName: "ContactTableViewCell", bundle: nil)
        tableView.register(contactNib, forCellReuseIdentifier: "contactCell")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addBackButton()
        configTableView()

        if strType != "MP3" {
            title = strType

            if strType == "Youtube" {
                
                fillTv(with: list)
                testData()
            }
            
        } else {
            if let img = bgImg {
                configBlurAndBgImg(of: tableView, with: img)
            }
        }
    }
    
    func testData(with id: String = "GteIM57f2u0") {
        mediaURL = URL(string: "https://www.youtube.com/watch?v=\(id)")
        thumbnailImageURL = URL(string: "https://img.youtube.com/vi/\(id)/mqdefault.jpg") //default.jpg
        
    }
    
    func fillTv(with list: [String]?) {
        if let videoIdList = list {
            
            for id in videoIdList {
                mediaUrlList.append(URL(string: "https://www.youtube.com/watch?v=\(id)")!)
                thumbnailImageUrlList.append(URL(string: "https://img.youtube.com/vi/\(id)/mqdefault.jpg")!)
            }
        }
    }

    
    func configBlurAndBgImg(of tableiew: UITableView, with img: UIImage) {
        
        tableView.backgroundView = UIImageView(image: img)
        
        let blurEffect = UIBlurEffect(style: .regular)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = tableView.bounds
        tableView.backgroundView?.addSubview(blurView)
        
    }
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var identifier = ""
        switch strType {
        case "Youtube":
            identifier = "youtubeCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! YoutubeTableViewCell
            cell.titleLabel?.text = "In direct cu Stefan Pusca \(list[indexPath.row])"
            cell.descriptionLabel?.text = "In acest clip vorbim despre meditatia \(list[indexPath.row]) si efectele acesteia asupra corpului si mintii "
            cell.thumbnailView?.kf.setImage(with: thumbnailImageUrlList[indexPath.row], placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageUrl) in
                if error == nil{
                    cell.indicator.stopAnimating()
                }else if error != nil{
                    cell.indicator.stopAnimating()
                }
            })
            print(thumbnailImageURL!)
            return cell
        case "MP3":
            identifier = "mp3Cell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! Mp3TableViewCell
            let song = songs[indexPath.row]
            cell.titleLabel.text = String(song.displayName)
            cell.accessoryType = .disclosureIndicator
            
            return cell
        default:
            //case: "Contact":
            if contactInfoList != nil {
                identifier = "contactCell"
            }
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as!  ContactTableViewCell
            cell.titleLabel.text = String((contactInfoList?[indexPath.row].title) ?? "Nil")
            cell.subtitleLabel.text = String((contactInfoList?[indexPath.row].subtitle) ?? "Nil")
            cell.imageView?.image = contactInfoList?[indexPath.row].icon

            return cell
        }

    }
    
   func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch strType {
    case "MP3":
        return songs.count
    case "Youtube":
        return list.count
    default:
        return contactInfoList?.count ?? 0
    }
        //return strType == "Contact" ? contactInfoList?.count ?? 0 : list.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return strType == "Youtube" ? 100 : 60
    }
    
    @IBOutlet weak var videoView: WKWebView!
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        selectedRow = indexPath.row
        mediaURL = mediaUrlList[indexPath.row]
        
        
        switch strType {
        case "Youtube":
            
            let request = URLRequest(url: mediaURL!)
            spinner.startAnimating()
            videoView.load(request)
            videoView.navigationDelegate = self
             
            
        case "MP3":
            
            //testing
//            performSegue(withIdentifier: "playMp3File", sender: self)
            
            //***working app***
            //present player
            var position = indexPath.row //to know which one is next

            guard let vc = storyboard?.instantiateViewController(identifier: "player") as?
                PlayerViewController else { return }
            
            //vc.modalPresentationStyle = .overCurrentContext
            
            vc.songs = songs
            vc.position = position
            //
            vc.changeBgFromPlayerBack = { result in
                position -= 1
                self.setBg(of: position)
            }
            vc.changeBgFromPlayerNext = { result in
                position += 1
                self.setBg(of: position)
            }

            //runs only when didSet (not in closures above)
            setBg(of: position)

            present(vc, animated: true)
        
        default:
            
            switch contactInfoList?[selectedRow!].title {
            
            case "Email":
                let recipientEmail = "baloi_radu@yahoo.com"
                let subject = "<Subiect>"
                let body = "This code supports sending email via multiple different email apps on iOS! :)"

                // Show default mail composer
                if MFMailComposeViewController.canSendMail() {
                    let mail = MFMailComposeViewController()
                    mail.mailComposeDelegate = self
                    mail.setToRecipients([recipientEmail])
                    mail.setSubject(subject)
                    mail.setMessageBody(body, isHTML: false)

                    present(mail, animated: true)

                // Show third party email composer if default Mail app is not present
                } else if let emailUrl = createEmailUrl(to: recipientEmail, subject: subject, body: body) {
                    UIApplication.shared.open(emailUrl)
                }
            
            case "Website":
                if let url = URL(string: "http://www.stefan-pusca.ro") {
                    UIApplication.shared.open(url)
                }
            
            default: //Name or something
                return
            }
          
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    fileprivate var selectedRow: Int?
    
    func setBg(of row: Int = 0) {
        configBlurAndBgImg(of: tableView, with: #imageLiteral(resourceName: songs[row].imageName))
       }
    
    
//    // MARK: - Navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//
////        if (segue.identifier == "playYtVideo") {
////            if let destinationVC = segue.destination as? UINavigationController {
////                if let targetController = destinationVC.topViewController as? YtPlayerViewController {
////                    targetController.mediaURL = mediaURL
////                }
////            }
////        } else
//
//            if (segue.identifier == "playMp3File") {
//            //if let destinationVC = segue.destination as? UINavigationController {
//                if let targetController = segue.destination as? Mp3PlayerViewController {
//                    //targetController.title = list[selectedRow!]
//                    //targetController.rowSelected = selectedRow
//                    let urlString = "https://open.spotify.com/track/3mgRwzfkoOQROm3ksVpypG"
//                    //targetController.modalPresentationStyle = .fullScreen
//                    targetController.mediaURL = URL(string: urlString)
//                //}
//            }
//        }
//    }

}

extension SelectedCategoryViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        spinner.stopAnimating()
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        spinner.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        spinner.stopAnimating()
    }
}

extension SelectedCategoryViewController: MFMailComposeViewControllerDelegate {
    private func createEmailUrl(to: String, subject: String, body: String) -> URL? {
        let subjectEncoded = subject.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        let bodyEncoded = body.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!

        let gmailUrl = URL(string: "googlegmail://co?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let outlookUrl = URL(string: "ms-outlook://compose?to=\(to)&subject=\(subjectEncoded)")
        let yahooMail = URL(string: "ymail://mail/compose?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let sparkUrl = URL(string: "readdle-spark://compose?recipient=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let defaultUrl = URL(string: "mailto:\(to)?subject=\(subjectEncoded)&body=\(bodyEncoded)")

        if let gmailUrl = gmailUrl, UIApplication.shared.canOpenURL(gmailUrl) {
            return gmailUrl
        } else if let outlookUrl = outlookUrl, UIApplication.shared.canOpenURL(outlookUrl) {
            return outlookUrl
        } else if let yahooMail = yahooMail, UIApplication.shared.canOpenURL(yahooMail) {
            return yahooMail
        } else if let sparkUrl = sparkUrl, UIApplication.shared.canOpenURL(sparkUrl) {
            return sparkUrl
        }

        return defaultUrl
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
