//
//  Mp3PlayerViewController.swift
//  meditation
//
//  Created by XeVoNx on 26/06/2020.
//  Copyright © 2020 XeVoNx. All rights reserved.
//

import UIKit
import AVFoundation

class Mp3PlayerViewController: UIViewController {
    
    //@IBOutlet weak var spinner: UIActivityIndicatorView!
    //@IBOutlet weak var imageView: UIImageView!
    
    var mediaURL: URL?
    var player: AVAudioPlayer!
    var rowSelected: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addBackButton()
        
        playSoundFromURL(mediaURL!)
    }
    
    func playSoundFromURL(_ url: URL?) {
        //spinner.startAnimating()
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            if let url = url, let urlData = NSData(contentsOf: url) {
                self?.player = try? AVAudioPlayer(data: urlData as Data)
                self?.player?.play()
                //DispatchQueue.main.async { [weak self] in
                    //self?.spinner.stopAnimating()
                //}
            }
        }
        
        
    }
    @IBAction func backButtonMP3(_ sender: UIButton) {
    }
    
    @IBAction func playButtonMP3(_ sender: UIButton) {
    }
    
    @IBAction func forwardButtonMP3(_ sender: UIButton) {
    }
    
    
}
